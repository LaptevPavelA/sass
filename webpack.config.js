let path = require ('path');
let sass = require ('./webpack/sass');
const merge = require('webpack-merge');

let conf = merge([
    {
    entry:'./src/index.js',
    output: {
        path: path.resolve(__dirname, './dist'),
        filename: 'main.js',
        publicPath: 'dist/'
    },
    },
    sass(),

]);


module.exports = conf;